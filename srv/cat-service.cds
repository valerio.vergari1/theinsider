using my.bookshop as my from '../db/data-model';

service CatalogService {

    @odata.draft.enabled
    entity Users as projection on my.Users;

    @odata.draft.enabled
    entity Books as projection on my.Books;

    @odata.draft.enabled
    entity Authors as projection on my.Authors;

    entity Users2Books as projection on my.Users2Books;

    entity Authors2Books as projection on my.Authors2Books;
}
