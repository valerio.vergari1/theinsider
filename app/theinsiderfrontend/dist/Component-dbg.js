sap.ui.define(
    ["sap/fe/core/AppComponent"],
    function (Component) {
        "use strict";

        return Component.extend("theinsiderfrontend.Component", {
            metadata: {
                manifest: "json"
            }
        });
    }
);