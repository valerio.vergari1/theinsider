sap.ui.require(
    [
        'sap/fe/test/JourneyRunner',
        'theinsiderfrontend/test/integration/FirstJourney',
		'theinsiderfrontend/test/integration/pages/UsersList',
		'theinsiderfrontend/test/integration/pages/UsersObjectPage'
    ],
    function(JourneyRunner, opaJourney, UsersList, UsersObjectPage) {
        'use strict';
        var JourneyRunner = new JourneyRunner({
            // start index.html in web folder
            launchUrl: sap.ui.require.toUrl('theinsiderfrontend') + '/index.html'
        });

       
        JourneyRunner.run(
            {
                pages: { 
					onTheUsersList: UsersList,
					onTheUsersObjectPage: UsersObjectPage
                }
            },
            opaJourney.run
        );
    }
);