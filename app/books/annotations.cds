using CatalogService as service from '../../srv/cat-service';

annotate service.Books with @(UI.LineItem: [
    {
        $Type: 'UI.DataField',
        Label: 'Title',
        Value: title,
    },
    {
        $Type : 'UI.DataField',
        Value : Author.name,
        Label : 'Name',
    },
    {
        $Type : 'UI.DataField',
        Value : Author.surname,
        Label : 'Surname',
    },
    {
        $Type: 'UI.DataField',
        Label: 'Author Id',
        Value: Author_ID,
    },
]);

annotate service.Books with @(
    UI.FieldGroup #GeneratedGroup1: {
        $Type: 'UI.FieldGroupType',
        Data : [
            {
                $Type: 'UI.DataField',
                Label: 'Title',
                Value: title,
            },
            {
                $Type: 'UI.DataField',
                Label: 'Author_id',
                Value: Author_ID,
            },

        ],
    },
    UI.Facets                     : [{
        $Type : 'UI.ReferenceFacet',
        ID    : 'GeneratedFacet1',
        Label : 'General Information',
        Target: '@UI.FieldGroup#GeneratedGroup1',
    }, ]
) {
    Author @(Common.ValueList: {
        CollectionPath: 'Authors',
        Parameters    : [
            {
                $Type            : 'Common.ValueListParameterOut',
                LocalDataProperty: 'Author_ID',
                ValueListProperty: 'ID',
            },
            {
                $Type            : 'Common.ValueListParameterDisplayOnly',
                ValueListProperty: 'name'
            },
            {
                $Type            : 'Common.ValueListParameterDisplayOnly',
                ValueListProperty: 'surname'
            }
        ]
    });
}


;
