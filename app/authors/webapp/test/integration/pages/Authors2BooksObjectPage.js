sap.ui.define(['sap/fe/test/ObjectPage'], function(ObjectPage) {
    'use strict';

    var CustomPageDefinitions = {
        actions: {},
        assertions: {}
    };

    return new ObjectPage(
        {
            appId: 'authors',
            componentId: 'Authors2BooksObjectPage',
            entitySet: 'Authors2Books'
        },
        CustomPageDefinitions
    );
});