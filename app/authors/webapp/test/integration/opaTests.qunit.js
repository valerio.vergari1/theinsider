sap.ui.require(
    [
        'sap/fe/test/JourneyRunner',
        'authors/test/integration/FirstJourney',
		'authors/test/integration/pages/AuthorsList',
		'authors/test/integration/pages/AuthorsObjectPage',
		'authors/test/integration/pages/Authors2BooksObjectPage'
    ],
    function(JourneyRunner, opaJourney, AuthorsList, AuthorsObjectPage, Authors2BooksObjectPage) {
        'use strict';
        var JourneyRunner = new JourneyRunner({
            // start index.html in web folder
            launchUrl: sap.ui.require.toUrl('authors') + '/index.html'
        });

       
        JourneyRunner.run(
            {
                pages: { 
					onTheAuthorsList: AuthorsList,
					onTheAuthorsObjectPage: AuthorsObjectPage,
					onTheAuthors2BooksObjectPage: Authors2BooksObjectPage
                }
            },
            opaJourney.run
        );
    }
);