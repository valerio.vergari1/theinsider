using CatalogService as service from '../../srv/cat-service';

annotate service.Authors2Books with @(UI.LineItem: [{
    $Type: 'UI.DataField',
    Label: 'Publication Date',
    Value: publishedAt,
},
    {
        $Type : 'UI.DataField',
        Value : author.Books.book.title,
        Label : 'Title',
    },
    {
        $Type : 'UI.DataField',
        Value : author.Books.book_ID,
        Label : 'Book Id',
    }]);

annotate service.Authors2Books with @(
    Common.SemanticKey            : [publishedAt],
    UI.FieldGroup #GeneratedGroup1: {
        $Type: 'UI.FieldGroupType',
        Data : [
            {
                $Type: 'UI.DataField',
                Label: 'Publication Date',
                Value: publishedAt,
            },
            {
                $Type: 'UI.DataField',
                Label: 'Author Id',
                Value: author_ID,
            },
            {
                $Type : 'UI.DataField',
                Value : author.name,
                Label : 'Name',
            },
            {
                $Type : 'UI.DataField',
                Value : author.surname,
                Label : 'Surname',
            },
            {
                $Type: 'UI.DataField',
                Label: 'Book Id',
                Value: book_ID,
            },
            {
                $Type : 'UI.DataField',
                Value : book.title,
                Label : 'Title',
            }
        ]
    },
    UI.Facets                     : [{
        $Type : 'UI.ReferenceFacet',
        ID    : 'GeneratedFacet1',
        Label : 'Published Book',
        Target: '@UI.FieldGroup#GeneratedGroup1',
    }]
) {
    author @(Common.ValueList: {
        CollectionPath: 'Authors',
        Parameters    : [
            {
                $Type            : 'Common.ValueListParameterOut',
                LocalDataProperty: 'author_ID',
                ValueListProperty: 'ID',
            },
            {
                $Type            : 'Common.ValueListParameterDisplayOnly',
                ValueListProperty: 'name'
            },
            {
                $Type            : 'Common.ValueListParameterDisplayOnly',
                ValueListProperty: 'surname'
            }
        ]
    });
    book   @(Common.ValueList: {
        CollectionPath: 'Books',
        Parameters    : [
            {
                $Type            : 'Common.ValueListParameterOut',
                LocalDataProperty: 'book_ID',
                ValueListProperty: 'ID',
            },
            {
                $Type            : 'Common.ValueListParameterDisplayOnly',
                ValueListProperty: 'title'
            },
        ]
    });
};

annotate service.Authors with @(UI.LineItem: [
    {
        $Type: 'UI.DataField',
        Label: 'Name',
        Value: name,
    },
    {
        $Type: 'UI.DataField',
        Label: 'Surname',
        Value: surname,
    },
]);

annotate service.Authors with @(
    UI.FieldGroup #GeneratedGroup1: {
        $Type: 'UI.FieldGroupType',
        Data : [
            {
                $Type: 'UI.DataField',
                Label: 'Name',
                Value: name,
            },
            {
                $Type: 'UI.DataField',
                Label: 'Surname',
                Value: surname,
            },
        ],
    },
    UI.Facets                     : [
        {
            $Type : 'UI.ReferenceFacet',
            ID    : 'GeneratedFacet1',
            Label : 'Author Information',
            Target: '@UI.FieldGroup#GeneratedGroup1',
        },
        {
            $Type : 'UI.ReferenceFacet',
            ID    : 'GeneratedFacet2',
            Label : 'Published Books',
            Target: 'Books/@UI.LineItem'
        },
    ]
);

annotate service.Authors with @(
    UI.HeaderInfo : {
        TypeName : 'Author',
        TypeNamePlural : 'Authors',
    }
);
