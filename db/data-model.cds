namespace my.bookshop;

@assert.unique: {
  name: [ name ],
  surname: [ surname ],
}
entity Users {
  key ID : UUID;
  name  : String;
  surname  : String;
  Books : Composition of many Users2Books on Books.user = $self;
}

entity Users2Books {
  key ID : UUID;
  user : Association to Users;
  book : Association to Books;
  quantity : Integer;
  createdAt : Timestamp @cds.on.insert: $now;
}

@assert.unique: {
  name: [ name ],
  surname: [ surname ],
}
entity Authors {
  key ID : UUID;
  name : String;
  surname : String;
  Books : Composition of many Authors2Books on Books.author = $self;
}

entity Authors2Books {
  key ID: UUID;
  author : Association to Authors;
  book : Association to Books;
  publishedAt : Timestamp;
}

entity Books {
  key ID : UUID;
  title : String;
  Author : Association to Authors not null;
}